#include <iostream>
#include <sstream>
#include <tipa/tinyparser.hpp>

using namespace std;
using namespace tipa;

bool calculator(stringstream& stream) {
    rule term, expr, operand, op_mul, op_sum;
    // an expression is an operand followed (optionally) by an op_mul & another operand
    expr = operand >> *(op_mul >> operand);
    // an operand is a num followed (optionally) by an op_sum and another term
    operand = term >> *(op_sum >> term);
    // tk_int is a predefined token for integers
    term = rule(tk_int);
    op_sum = rule('+') | rule('-');
    op_mul = rule('*') | rule('/');

    parser_context pc;
    pc.set_stream(stream);
    bool res;
    try {
        res = expr.parse(pc);
    } catch(...) {}

    if (!res) {
        cout << pc.get_formatted_err_msg();
    }

    return res;
}

int main(int argc, char**argv) {
    if (argc == 2) {
        stringstream ss(argv[1]);
        bool res = calculator(ss);
        if (!res) {
            return 1;
        }
    }

    return 0;
}
